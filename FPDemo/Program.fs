﻿// Learn more about F# at http://fsharp.net
// See the 'F# Tutorial' project for more help.

open System

let assignDemo =

    let number = 1

    let newNumber = number + 1

    let message = "Hello"

    let fullMessage = message + " World"

    fullMessage

let functionsDemo =
    let sum a b = 
        a + b
    0

let parseIntergerDemo (value:string) =
   match System.Int32.TryParse value with
       | (true,int) -> Some(int)
       | _ -> None     



// Factorial (0) = 1
// Factorial (1) = 1 * Factorial (0)
// Factorial (2) = 2 * Factorial (1)
// Factorial (3) = 3 * Factorial (2)
// Factorial (N) = N * Factorial (N - 1)
let rec factorial1 n =
    if n = 0 then
        1
    else
        n * factorial1 (n-1)


let rec factorial2 n =
    match n with
    | 0 -> 1
    | _ -> n * factorial2(n-1)

let rec addUpListDemo list =
    match list with 
    | head::tail -> head + (addUpListDemo tail)
    | [] -> 0

let square x = x * x

let playWithListDemo() =
    let numbers = [1..3] // [1;2;3]
    let squaredList = List.map square numbers
    List.iter (printfn "%i") squaredList

let playWithListDemoWithPipeline() =
    [1..3] |> List.map square |> List.iter (printfn "%i")


[<EntryPoint>]
let main argv = 
    printfn "%A" (factorial1 2)
    0 // return an integer exit code
