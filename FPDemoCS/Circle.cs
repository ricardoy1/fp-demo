﻿namespace FPDemoCS
{
    public class Circle
    {
        public int Id { get; private set; }
        public double Diameter { get; private set; }

        public Circle(double diameter)
        {
            this.Diameter = diameter;
        }

        public Circle SetNewDiameter(double diameter)
        {
            this.Diameter = diameter;
            return this;
        }

        public bool IsBig()
        {
            return this.Diameter > 10.0;
        }

        public Circle Save()
        {
            // CircleRepository.Save(circle);
            this.Id = 123;
            return this;
        }
    }
}
