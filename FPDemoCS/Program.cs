﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPDemoCS
{
    class Program
    {
        static void Main(string[] args)
        {
            Func<int, int, int> x = Sum;
            x(1, 2);
        }

        public static int Sum(int a, int b)
        {
            return a + b;
        }

        public static IEnumerable<Circle> GetBigCirclesFunctional(IEnumerable<Circle> circles)
        {
            return circles.Where(c => c.IsBig());
        }

        public static IEnumerable<Circle> GetBigCirclesImperative(IEnumerable<Circle> circles)
        {
            var bigCircles = new List<Circle>();
            foreach (var circle in circles)
            {
                if (circle.IsBig())
                {
                    bigCircles.Add(circle);
                }
            }

            return bigCircles;
        }

        public static Circle Imperative()
        {
            var circle = new Circle(diameter: 10.0);

            circle.SetNewDiameter(20.0);

            circle.Save();

            return circle;
        }

        public static Circle Functional()
        {
            var circle = new Circle(diameter: 10.0);

            var newCircle = circle.SetNewDiameter(20.0);

            var savedCircle = newCircle.Save();

            return savedCircle;
        }
    }
}
